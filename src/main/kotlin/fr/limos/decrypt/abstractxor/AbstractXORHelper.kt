package fr.limos.decrypt.abstractxor

import fr.limos.decrypt.abstractxor.utils.Cluster
import fr.limos.decrypt.abstractxor.utils.UnionFind
import org.chocosolver.solver.Model
import org.chocosolver.solver.constraints.Constraint
import org.chocosolver.solver.variables.IntVar

object AbstractXORHelper {
    fun postAbsXor(
        model: Model,
        consistency: Consistency,
        equations: List<Array<IntVar>>
    ) {
        val clusters = makeClusters(equations)
        for (cluster in clusters) {
            val propagator = when (consistency) {
                Consistency.Feas -> AbstractXORFeas(cluster.variables, cluster.equations.map { it.toList() })
                Consistency.Gac -> AbstractXORGac(cluster.variables, cluster.equations.map { it.toList() })
            }
            model.post(Constraint("AbstractXOR($consistency)", propagator))
        }
    }

    private fun makeClusters(equations: List<Array<IntVar>>): List<Cluster> {
        val representatives = UnionFind<IntVar>()

        for (equation in equations) {
            for (i in 1 until equation.size) {
                representatives.union(equation[0], equation[i])
            }
        }

        val clusters = mutableMapOf<IntVar, Cluster>()

        for (equation in equations) {
            val representative = representatives.find(equation[0])
            val cluster = clusters.getOrPut(representative) { Cluster() }
            for (variable in equation) {
                if(variable !in cluster.variables) {
                    cluster.variables += variable
                }
            }
            cluster.equations += equation
        }


        return clusters.values
            .toList()
    }
}

