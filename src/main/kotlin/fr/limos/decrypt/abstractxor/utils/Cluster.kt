package fr.limos.decrypt.abstractxor.utils

import org.chocosolver.solver.variables.IntVar

data class Cluster(
    val variables: MutableList<IntVar> = mutableListOf(),
    val equations: MutableList<Array<IntVar>> = mutableListOf()
)