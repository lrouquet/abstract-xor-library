package fr.limos.decrypt.abstractxor;

import fr.limos.decrypt.abstractxor.structures.api.ISparseMatrix;
import fr.limos.decrypt.abstractxor.structures.impl.SparseMatrix;
import org.chocosolver.memory.IEnvironment;
import org.chocosolver.solver.constraints.Propagator;
import org.chocosolver.solver.constraints.PropagatorPriority;
import org.chocosolver.solver.exception.ContradictionException;
import org.chocosolver.solver.variables.IntVar;
import org.chocosolver.solver.variables.events.PropagatorEventType;
import org.chocosolver.util.ESat;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.PrimitiveIterator;

public class AbstractXORFeas extends Propagator<IntVar> {

    private final List<List<? extends IntVar>> initEquations;
    private final ISparseMatrix m;
    private final boolean ASSERTIONS_ENABLED;

    public AbstractXORFeas(List<? extends IntVar> vars, List<List<? extends IntVar>> initEquations) {
        super(vars.toArray(new IntVar[0]), PropagatorPriority.CUBIC, true);
        IEnvironment env = getModel().getEnvironment();
        this.initEquations = initEquations;
        m = new SparseMatrix(env, initEquations.size(), vars.size());
        ASSERTIONS_ENABLED = getClass().desiredAssertionStatus();
    }

    private boolean _isFalse(IntVar var) {
        return var.isInstantiatedTo(0);
    }

    public boolean isFalse(int iVar) {
        return _isFalse(vars[iVar]);
    }

    private void _setFalse(IntVar var) throws ContradictionException {
        var.instantiateTo(0, this);
    }

    private void setFalse(int iVar) throws ContradictionException {
        _setFalse(vars[iVar]);
    }

    private boolean _isTrue(IntVar var) {
        return var.getLB() > 0;
    }

    public boolean isTrue(int iVar) {
        return _isTrue(vars[iVar]);
    }

    @Override
    public void propagate(int evtmask) throws ContradictionException {
        if (PropagatorEventType.isFullPropagation(evtmask)) {
            prepare();
        }
        RRE();
        if (ASSERTIONS_ENABLED) ASSERT_RRE_FORM();
        checkAtLeast2();
        if (ASSERTIONS_ENABLED) ASSERT_CHECK_AT_LEAST_2();
    }

    @Override
    public void propagate(int idxVarInProp, int mask) throws ContradictionException {
        if (isFalse(idxVarInProp)) {
            m.clearCol(idxVarInProp);
        }
        forcePropagate(PropagatorEventType.CUSTOM_PROPAGATION);
    }

    @Override
    public ESat isEntailed() {
        boolean allInit = true;
        for (ISparseMatrix.IRow row : m.rows()) {
            int nonPivot = row.size() - 1;
            assert nonPivot >= 0;
            if (nonPivot == 0 && isTrue(row.pivot())) return ESat.FALSE;
            for (int iVar : row) {
                allInit &= isTrue(iVar);
            }
        }
        return allInit ? ESat.TRUE : ESat.UNDEFINED;
    }

    private int indexOf(IntVar v) {
        int index = vars.length - 1;
        while (index >= 0 && vars[index] != v) {
            index -= 1;
        }
        return index;
    }

    private void prepare() {
        for (int i = 0; i < initEquations.size(); i++) {
            List<? extends IntVar> currEq = initEquations.get(i);
            for (IntVar var : currEq) {
                if (!_isFalse(var)) {
                    int iVar = indexOf(var);
                    m.insert(i, iVar);
                }
            }
        }
    }

    private int nbNonPivot(ISparseMatrix.IRow row) {
        int nbNonPivot = row.size() - 1;
        assert nbNonPivot >= 0;
        return nbNonPivot;
    }

    private void RRE() throws ContradictionException {
        for (ISparseMatrix.IRow rowI : m.rows()) {
            if (rowI.isEmpty()) {
                rowI.remove();
            } else {
                int pivot = rowI.pivot();
                PrimitiveIterator.OfInt iter = m.col(pivot).iterator();
                while (iter.hasNext()) {
                    ISparseMatrix.IRow rowJ = m.row(iter.next());
                    if (rowJ != rowI) {
                        m.xor(rowJ.iRow(), rowI.iRow());
                        if (rowJ.isEmpty()) {
                            rowJ.remove();
                        } else if (rowJ.size() == 1 && isTrue(rowJ.pivot())) {
                            fails();
                        }
                    }
                }
            }
        }
    }

    private void checkAtLeast2() throws ContradictionException {
        for (ISparseMatrix.IRow row : m.rows()) {
            int nbNonPivot = nbNonPivot(row);
            if (nbNonPivot == 0) {
                int pivot = row.pivot();
                if (isTrue(pivot)) {
                    fails();
                } else {
                    assert m.col(pivot).size() == 1;
                    setFalse(pivot);
                    // m.clearCol(pivot);
                    // It's no necessary to clear the column since the pivot appears only in one row
                    // and this row will be deleted
                    row.remove();
                }
            }
        }
    }

    private void ASSERT_RRE_FORM() {
        for (ISparseMatrix.IRow eq : m.rows()) {
            assert m.col(eq.pivot()).size() == 1 || _db(true);
            assert eq.isNotEmpty() || _db(true);
        }
    }

    private void ASSERT_CHECK_AT_LEAST_2() {
        for (ISparseMatrix.IRow row : m.rows()) {
            int size = row.size();
            assert size >= 2;
        }
    }

    private boolean _db(boolean exit) {
        File out = new File("~/db.txt");
        try (FileWriter w = new FileWriter(out)) {
            w.write(m.toString());
        } catch (IOException ioe) {
        }
        return !exit;
    }

    private void dbg() {
        assert _db(false);
    }

}

