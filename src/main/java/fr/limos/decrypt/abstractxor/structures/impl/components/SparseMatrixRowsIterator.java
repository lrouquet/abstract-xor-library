package fr.limos.decrypt.abstractxor.structures.impl.components;

import fr.limos.decrypt.abstractxor.structures.api.ISparseMatrix;

import java.util.Iterator;

public class SparseMatrixRowsIterator implements Iterator<ISparseMatrix.IRow> {

   private final Root root;
   private Row current;

   public SparseMatrixRowsIterator(Root root) {
      this.root = root;
      this.current = root.next;
   }

   public void reset() {
      current = root.next;
   }

   @Override public boolean hasNext() {
      return current != root;
   }

   @Override public Row next() {
      Row result = current;
      current = current.next;
      return result;
   }
}
