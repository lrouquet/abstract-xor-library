package fr.limos.decrypt.abstractxor.structures.api;

import java.util.function.IntPredicate;

/**
 * Represents a sparse boolean matrix
 */
public interface ISparseMatrix {

    /**
     * Remove all the values in the column iCol
     * @param iCol the column to clear
     */
    void clearCol(int iCol);

    /**
     * Set M[iRow, iCol] to true
     * @param iRow the row index
     * @param iCol the column index
     */
    void insert(int iRow, int iCol);

    /**
     * Perform an inplace operation such as M[iTarget] = M[iTarget] xor M[iPivot]
     * @param iTarget the row target
     * @param iPivot the row pivot
     */
    void xor(int iTarget, int iPivot);

    /**
     * Return the iRow th row of the matrix
     * @param iRow the index of the row
     * @return the iRow th row of the matrix
     */
    IRow row(int iRow);

    /**
     * Return the iCol th column of the matrix
     * @param iCol the index of the column
     * @return the iCol th column of the matrix
     */
    IColumn col(int iCol);

    /**
     * Return all the rows of the matrix
     * @return the rows of the matrix
     */
    IRows rows();

    /**
     * Represents an iterable interface other the rows of the matrix
     */
    interface IRows extends Iterable<IRow> {}

    /**
     * Represents a row in the sparse matrix
     */
    interface IRow extends ISparseSet {
        /**
         * Remove the row from the matrix
         */
        void remove();

        /**
         * Return the index of the row
         * @return the index of the row
         */
        int iRow();

        /**
         * Return the pivot of the row (i.e. there is not other row in the matrix that contains pivot).
         * @return the pivot of the row
         */
        int pivot();

        /**
         * Return the number of elements i \in row such as predicate.test(i) == true
         * @param predicate the predicate
         * @return the number of elements in row that assert the predicate
         */
        int count(IntPredicate predicate);

        /**
         * Return true if the two equations have the same variables without there bases
         * @return true if this \ {pivot} == other \ {other.pivot}
         */
        boolean hasSameNonPivotVariables(IRow other);

        /**
         * Return the next non empty row in the current matrix. The next row may be the end of the matrix (m.end()).
         * @return the next non empty row in the current matrix
         */
        IRow next();
    }

    /**
     * Represents an header to the rows. An IRoot is not a valid row.
     * Example of use:
     * IRow row;
     * while (!(row instanceof IRoot)) { row = row.next(); }
     */
    interface IRoot extends IRow {}

    /**
     * Represents a column in the current matrix
     */
    interface IColumn extends ISparseSet {
        int iCol();
    }

}
