package fr.limos.decrypt.abstractxor.structures.impl.components;


import fr.limos.decrypt.abstractxor.structures.api.ISparseMatrix;
import fr.limos.decrypt.abstractxor.structures.impl.SparseSet;

public class Column extends SparseSet implements ISparseMatrix.IColumn {

   private final int iCol;

   public Column(int iCol, int cap) {
      super(cap);
      this.iCol = iCol;
   }

   @Override public int iCol() {
      return iCol;
   }

   @Override
   public String toString() {
      return "(Column=" + iCol + ", values=" + super.toString() + ')';
   }
}
