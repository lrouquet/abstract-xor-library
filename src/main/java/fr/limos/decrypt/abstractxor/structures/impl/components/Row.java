package fr.limos.decrypt.abstractxor.structures.impl.components;

import fr.limos.decrypt.abstractxor.structures.api.ISparseMatrix;
import fr.limos.decrypt.abstractxor.structures.impl.SparseSet;
import org.chocosolver.memory.IEnvironment;
import org.chocosolver.memory.structure.IOperation;

import java.util.PrimitiveIterator;
import java.util.function.IntPredicate;

public class Row extends SparseSet implements ISparseMatrix.IRow {

   public Row prev;
   public Row next;

   private final IEnvironment env;
   private final int iRow;
   protected Root root;
   private final RevRemove REV_REMOVE_ARENA;

   static class RevRemove implements IOperation {
      Row current;

      RevRemove(Row current) {
         this.current = current;
      }

      @Override
      public void undo() {
         current.prev.next = current;
         current.next.prev = current;
      }
   }

   public Row(IEnvironment env, Root root, int iRow, int cap) {
      super(cap);
      this.env = env;
      this.root = root;
      this.iRow = iRow;
      this.prev = null;
      this.next = null;
      REV_REMOVE_ARENA = new RevRemove(this);
   }

   @Override
   public void remove() {
      prev.next = next;
      next.prev = prev;

      env.save(REV_REMOVE_ARENA);
   }

   @Override
   public int pivot() {
      return (size > 0) ? dom[0] : -1;
   }

   @Override
   public int count(IntPredicate predicate) {
      int count = 0;
      for (int i = 0; i < size; i++) {
         if (predicate.test(dom[i])) {
            count += 1;
         }
      }
      return count;
   }

   @Override
   public boolean hasSameNonPivotVariables(ISparseMatrix.IRow other) {
      if (size != other.size()) return false;
      PrimitiveIterator.OfInt it = other.iterator();
      int otherPivot = other.pivot();
      while (it.hasNext()) {
         int var = it.next();
         if (var != otherPivot && !contains(var)) {
            return false;
         }
      }
      return true;
   }

   @Override
   public ISparseMatrix.IRow next() {
      return next;
   }

   @Override
   public int iRow() {
      return iRow;
   }

   @Override
   public String toString() {
      return "(Row=" + iRow + ", values=" + super.toString() + ')';
   }
}
