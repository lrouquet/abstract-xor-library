package fr.limos.decrypt.abstractxor.structures.impl.iterators;

import java.util.NoSuchElementException;
import java.util.PrimitiveIterator;

public class EmptySparseSetIterator implements PrimitiveIterator.OfInt {
   @Override public boolean hasNext() {
      return false;
   }

   @Override public int nextInt() {
      throw new NoSuchElementException("Empty iterator");
   }
}