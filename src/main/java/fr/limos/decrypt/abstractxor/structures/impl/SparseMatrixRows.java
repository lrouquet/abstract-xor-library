package fr.limos.decrypt.abstractxor.structures.impl;

import fr.limos.decrypt.abstractxor.structures.api.ISparseMatrix;
import fr.limos.decrypt.abstractxor.structures.impl.components.Root;
import fr.limos.decrypt.abstractxor.structures.impl.components.SparseMatrixRowsIterator;
import org.jetbrains.annotations.NotNull;

/**
 * An iterable structure other ISparseMatrix.IRows
 */
public class SparseMatrixRows implements ISparseMatrix.IRows {

    private final SparseMatrixRowsIterator[] ITERATOR_ARENA;
    private int current;

    public SparseMatrixRows(Root root) {
        ITERATOR_ARENA = new SparseMatrixRowsIterator[10];
        for (int i = 0; i < ITERATOR_ARENA.length; i++) {
            ITERATOR_ARENA[i] = new SparseMatrixRowsIterator(root);
        }
        current = 0;
    }

    @NotNull @Override public SparseMatrixRowsIterator iterator() {
        SparseMatrixRowsIterator iterator = ITERATOR_ARENA[current];
        if (current == ITERATOR_ARENA.length - 1) {
            current = 0;
        } else {
            current += 1;
        }
        iterator.reset();
        return iterator;
    }

}
