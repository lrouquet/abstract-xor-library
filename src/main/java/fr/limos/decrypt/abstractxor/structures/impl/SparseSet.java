package fr.limos.decrypt.abstractxor.structures.impl;

import fr.limos.decrypt.abstractxor.structures.api.ISparseSet;
import fr.limos.decrypt.abstractxor.structures.impl.iterators.EmptySparseSetIterator;
import fr.limos.decrypt.abstractxor.structures.impl.iterators.SparseSetIterator;
import org.jetbrains.annotations.NotNull;

import java.util.PrimitiveIterator;

public class SparseSet implements ISparseSet {

    protected static final EmptySparseSetIterator EMPTY_SPARSE_SET_ITERATOR = new EmptySparseSetIterator();
    protected int[] map;
    protected int[] dom;
    protected int size;

    public SparseSet(int capacity) {
        size = 0;
        map = new int[capacity];
        dom = new int[capacity];
        for (int i = 0; i < capacity; i++) {
            map[i] = i;
            dom[i] = i;
        }
    }

    protected void swap(int i, int j) {
        int tmp = dom[j];
        dom[j] = dom[i];
        dom[i] = tmp;

        map[dom[i]] = i;
        map[dom[j]] = j;
    }

    protected void insert(int i) {
        assert map[i] >= size;
        swap(map[i], size++);
    }

    protected void delete(int i) {
        assert map[i] < size;
        swap(map[i], --size);
    }

    @Override public boolean contains(int i) {
        return map[i] < size;
    }

    @Override public int get(int i) {
        if (i >= size) throw new IndexOutOfBoundsException(String.format("index: %d, size: %d", i, size));
        return dom[i];
    }

    @Override public int size() {
        return size;
    }

    @NotNull
    @Override public PrimitiveIterator.OfInt iterator() {
        return size <= 0 ? EMPTY_SPARSE_SET_ITERATOR : new SparseSetIterator(dom, size);
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder("{");
        for (int i = 0; i < size; i++) {
            str.append(dom[i]);
            str.append(", ");
        }
        if (size > 0) {
            str.setLength(str.length() - 2);
        }
        str.append("}");
        return str.toString();
    }
}