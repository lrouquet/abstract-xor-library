package fr.limos.decrypt.abstractxor.structures.api;

import fr.limos.decrypt.abstractxor.utils.PrimitiveIterable;

public interface ISparseSet extends PrimitiveIterable.OfInt  {

    /**
     * Return the number of elements in the sparse set
     * @return the number of elements in the sparse set
     */
    int size();

    /**
     * Return the ith element in the sparse set
     * precondition: 0 <= nth < size()
     * @param i the ith element in the sparse set
     * @return the nth element in the sparse set
     */
    int get(int i);

    /**
     * Return true if the set contains the element i
     * @param i the element to search
     * @return true if the set contains i else otherwise
     */
    boolean contains(int i);

    /**
     * Return true if the set is empty, false otherwise
     * @return true if the set is empty, false otherwise
     */
    default boolean isEmpty() {
        return size() <= 0;
    }

    /**
     * Return false if the set is empty, true otherwise
     * @return false if the set is empty, true otherwise
     */
    default boolean isNotEmpty() {
        return size() > 0;
    }

}
