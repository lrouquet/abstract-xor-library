package fr.limos.decrypt.abstractxor.structures.impl.components;

import fr.limos.decrypt.abstractxor.structures.api.ISparseMatrix;

public class Root extends Row implements ISparseMatrix.IRoot {

   public Root() {
      super(null, null, -1, 0);
      root = this;
   }

}