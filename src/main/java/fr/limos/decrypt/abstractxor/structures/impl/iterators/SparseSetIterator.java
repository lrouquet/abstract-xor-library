package fr.limos.decrypt.abstractxor.structures.impl.iterators;

import java.util.Arrays;
import java.util.PrimitiveIterator;

public class SparseSetIterator implements PrimitiveIterator.OfInt {

   private int i = 0;
   private final int[] values;

   public SparseSetIterator(int[] dom, int size) {
      values = Arrays.copyOf(dom, size);
   }

   @Override public boolean hasNext() {
      return i < values.length;
   }

   @Override public int nextInt() {
      return values[i++];
   }
}