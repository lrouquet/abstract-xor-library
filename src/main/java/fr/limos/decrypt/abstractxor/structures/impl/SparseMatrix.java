package fr.limos.decrypt.abstractxor.structures.impl;

import fr.limos.decrypt.abstractxor.structures.api.ISparseMatrix;
import fr.limos.decrypt.abstractxor.structures.impl.components.Column;
import fr.limos.decrypt.abstractxor.structures.impl.components.Root;
import fr.limos.decrypt.abstractxor.structures.impl.components.Row;
import org.chocosolver.memory.IEnvironment;
import org.chocosolver.memory.structure.IOperation;

public class SparseMatrix implements ISparseMatrix {

    /**
     * The initial height of the matrix
     */
    public final int N;

    /**
     * The initial width of the matrix
     */
    public final int M;

    private final IEnvironment env;
    private final Row[] rows;
    private final Column[] columns;
    private final SparseMatrixRows rowIter;
    private final ReverseXOR[][] REV_XOR_ARENA;
    private final ReverseRemCol[] REV_REM_COL_ARENA;
    private final Root root;

    class ReverseRemCol implements IOperation {
        private final Column col;
        ReverseRemCol(Column col) {
            this.col = col;
        }

        @Override public void undo() {
            _RESTORE_COL_(col);
        }
    }

    class ReverseXOR implements IOperation {
        private final Row target;
        private final Row pivot;

        ReverseXOR(Row target, Row pivot) {
            this.target = target;
            this.pivot = pivot;
        }

        @Override public void undo() {
            _XOR_(target, pivot);
        }
    }

    public SparseMatrix(IEnvironment env, int n, int m) {
        this.env = env;
        this.N = n;
        this.M = m;
        rows = new Row[n];
        this.root = new Root();
        for (int iEq = 0; iEq < n; iEq++) {
            rows[iEq] = new Row(env, root, iEq, m);
        }
        root.next = rows[0];
        root.prev = rows[n - 1];
        for (int iEq = 0; iEq < n; iEq++) {
            if (iEq == n - 1) {
                rows[iEq].next = root;
            } else {
                rows[iEq].next = rows[iEq + 1];
            }
            if (iEq == 0) {
                rows[iEq].prev = root;
            } else {
                rows[iEq].prev = rows[iEq - 1];
            }
        }
        rowIter = new SparseMatrixRows(root);

        columns = new Column[m];
        for (int iVar = 0; iVar < m; iVar++) {
            columns[iVar] = new Column(iVar, n);
        }

        REV_XOR_ARENA = new ReverseXOR[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                REV_XOR_ARENA[i][j] = new ReverseXOR(rows[i], rows[j]);
            }
        }

        REV_REM_COL_ARENA = new ReverseRemCol[m];
        for (int j = 0; j < m; j++) {
            REV_REM_COL_ARENA[j] = new ReverseRemCol(columns[j]);
        }
    }

    @Override public void clearCol(int iCol) {
        Column col = columns[iCol];
        _CLEAR_COL_(col);
        env.save(REV_REM_COL_ARENA[iCol]);
    }

    @Override public void insert(int iRow, int iCol) {
        if (rows[iRow].contains(iCol)) return;
        _INSERT_(iRow, iCol);
        env.save(() -> _DELETE_(iRow, iCol));
    }

    @Override public void xor(int iTarget, int iPivot) {
        final Row pivot = rows[iPivot];
        final Row target = rows[iTarget];
        _XOR_(target, pivot);
        env.save(REV_XOR_ARENA[iTarget][iPivot]);
    }

    @Override public IRow row(int iRow) {
        return rows[iRow];
    }

    @Override public IColumn col(int iCol) {
        return columns[iCol];
    }

    @Override public SparseMatrixRows rows() {
        return rowIter;
    }

    private void _INSERT_(int iRow, int iCol) {
        assert !rows[iRow].contains(iCol);
        rows[iRow].insert(iCol);
        columns[iCol].insert(iRow);
    }

    private void _DELETE_(int iRow, int iCol) {
        assert rows[iRow].contains(iCol);
        rows[iRow].delete(iCol);
        columns[iCol].delete(iRow);
    }

    private void _CLEAR_COL_(Column column) {
        for (int i = 0; i < column.size; i++) {
            rows[column.dom[i]].delete(column.iCol());
        }
        column.size = -column.size;
    }

    private void _RESTORE_COL_(Column column) {
        column.size = -column.size;
        for (int i = 0; i < column.size; i++) {
            rows[column.dom[i]].insert(column.iCol());
        }
    }

    private void _XOR_(Row target, Row pivot) {
        int iRow = target.iRow();
        for (int i = 0; i < pivot.size; i++) {
            int iCol = pivot.dom[i];
            if (target.contains(iCol)) {
                target.delete(iCol);
                columns[iCol].delete(iRow);
            } else {
                target.insert(iCol);
                columns[iCol].insert(iRow);
            }
        }
    }

    @Override public String toString() {
        StringBuilder str = new StringBuilder();

        str.append("Matrix[").append(N).append(", ").append(M).append("]\n");
        for (IRow row : rowIter) {
            str.append("row_").append(row.iRow()).append(": {");
            for (int iCol : row) {
                str.append(iCol).append(", ");
            }
            if (row.isNotEmpty()) {
                str.setLength(str.length() - 2);
            }
            str.append("}\n");
        }

        str.append("---\n");

        for (int j = 0; j < columns.length; j++) {
            str.append("col_").append(j).append(": {");
            for (int iRow : columns[j]) {
                str.append(iRow).append(", ");
            }
            if (columns[j].isNotEmpty()) {
                str.setLength(str.length() - 2);
            }
            str.append("}\n");
        }

        return str.toString();
    }

}
