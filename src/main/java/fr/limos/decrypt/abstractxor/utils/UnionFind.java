package fr.limos.decrypt.abstractxor.utils;

import java.util.HashMap;
import java.util.Map;

public class UnionFind<T> {

    private final Map<T, T> parentOf = new HashMap<>();

    public T find(T variable) {
        T parent = parentOf.get(variable);
        if (parent == null) return variable;
        else return find(parent);
    }

    public void union(T lhs, T rhs) {
        T lhsRoot = find(lhs);
        T rhsRoot = find(rhs);
        if (lhsRoot != rhsRoot) {
            parentOf.put(lhsRoot, rhsRoot);
        }
    }

}
