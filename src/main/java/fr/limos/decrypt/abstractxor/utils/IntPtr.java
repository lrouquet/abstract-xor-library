package fr.limos.decrypt.abstractxor.utils;

public class IntPtr {

    public int deref;

    public IntPtr() {
        this.deref = 0;
    }

    public IntPtr(int value) {
        this.deref = value;
    }

}

