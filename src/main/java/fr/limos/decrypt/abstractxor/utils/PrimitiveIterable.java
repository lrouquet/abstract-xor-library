package fr.limos.decrypt.abstractxor.utils;

import org.jetbrains.annotations.NotNull;

import java.util.PrimitiveIterator;
import java.util.function.IntConsumer;

public interface PrimitiveIterable<TT, T_CONS> extends Iterable<TT> {

    @NotNull
    @Override
    PrimitiveIterator<TT, T_CONS> iterator();

    interface OfInt extends PrimitiveIterable<Integer, IntConsumer> {

        @NotNull @Override PrimitiveIterator.OfInt iterator();

    }

}

