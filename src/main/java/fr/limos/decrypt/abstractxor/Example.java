package fr.limos.decrypt.abstractxor;

import org.chocosolver.cutoffseq.LubyCutoffStrategy;
import org.chocosolver.solver.Model;
import org.chocosolver.solver.Solver;
import org.chocosolver.solver.search.strategy.Search;
import org.chocosolver.solver.variables.BoolVar;
import org.chocosolver.solver.variables.IntVar;
import org.chocosolver.util.tools.ArrayUtils;

import java.util.ArrayList;
import java.util.List;

import static java.lang.reflect.Array.newInstance;

public class Example {

    Model m = new Model();
    final List<IntVar[]> xorEquations = new ArrayList<>();

    // Main encryption
    BoolVar[][] plainText;
    BoolVar[][][] X;
    BoolVar[][][] SX;
    BoolVar[][][] Y;
    BoolVar[][][] Z;
    BoolVar[][] cipherText;

    // KeySchedule
    BoolVar[][][] K;
    BoolVar[][] WK;

    public Example(int r, int sb, int version) {

        // Main encryption
        plainText = m.boolVarMatrix("plain_text", 4, 4);
        X = new BoolVar[r][][];
        SX = new BoolVar[r][][];
        Y = new BoolVar[r][4][4];
        Z = new BoolVar[r - 1][][];
        cipherText = m.boolVarMatrix("cipher_text", 4, 4);

        for (int i = 0; i < r; i++) {
            X[i] = m.boolVarMatrix("X[" + i + "]", 4, 4);
            SX[i] = m.boolVarMatrix("SX[" + i + "]", 4, 4);
            if (i < r - 1) {
                Z[i] = m.boolVarMatrix("Z[" + i + "]", 4, 4);
            }
        }

        if (version == 64) {
            K = new BoolVar[][][]{
                    m.boolVarMatrix("K0", 4, 4),
                    m.boolVarMatrix("K1", 4, 4)
            };
            WK = m.boolVarMatrix("WK", 4, 4);
            for (int j = 0; j < 4; j++) {
                for (int k = 0; k < 4; k++) {
                    addXor(WK[j][k], K[0][j][k], K[1][j][k]);
                }
            }
        } else { // version == 128
            K = new BoolVar[][][]{
                    m.boolVarMatrix("K", 4, 4)
            };
            WK = K[0];
        }

        ark(X[0], plainText, WK);
        for (int i = 0; i < r - 1; i++) {
            // δSX[i] = SBox(δX[i])
            subCell(SX[i], X[i]);
            // δY[i] = shuffleCell(δSX[i])
            shuffleCell(Y[i], SX[i]);
            // δZ[i] = mixColumn(δY[i])
            mixColumns(Z[i], Y[i]);
            // δX[i + 1] = δZ[i] xor δK[i]
            if (version == 64) {
                ark(X[i + 1], Z[i], K[i % 2]);
            } else { // version == 128
                ark(X[i + 1], Z[i], K[0]);
            }
        }
        // δSX[r-1] = SBox(δX[r-1])
        subCell(SX[r - 1], X[r - 1]);
        // δCipherText = δSX[r - 1] xor δWK
        ark(cipherText, SX[r - 1], WK);

        AbstractXORHelper.INSTANCE.postAbsXor(m, Consistency.Gac, xorEquations);
        BoolVar[] flattenX = ArrayUtils.flatten(X);

        IntVar[] nbActives = m.intVarArray(r, 0, sb);
        for (int i = 0; i < r; i++) {
            m.sum(ArrayUtils.flatten(X[i]), "=", nbActives[i]).post();
        }
        m.sum(nbActives, "=", sb).post();
        m.arithm(nbActives[0], ">", 0).post();

        for (int i = 3; i < r; i++) {
            IntVar sumToI = m.intVar(i, sb);
            m.sum(take(nbActives, i), "=", sumToI).post();
        }

        configure(nbActives, flattenX);
        Solver s = m.getSolver();
        while (s.solve()) {
            s.printShortStatistics();
        }
        s.printShortStatistics();

    }

    private <T> T[] take(T[] in, int n) {
        T[] res = (T[]) newInstance(in[0].getClass(), Math.min(in.length, n));
        if (n >= 0) System.arraycopy(in, 0, res, 0, n);
        return res;
    }

    private void ark(BoolVar[][] X1, BoolVar[][] X0, BoolVar[][] WK) {
        for (int j = 0; j < 4; j++) {
            for (int k = 0; k < 4; k++) {
                addXor(X1[j][k], X0[j][k], WK[j][k]);
            }
        }
    }

    private void subCell(BoolVar[][] SX, BoolVar[][] X) {
        for (int j = 0; j < 4; j++) {
            for (int k = 0; k < 4; k++) {
                m.arithm(X[j][k], "=", SX[j][k]).post();
            }
        }
    }

    private void shuffleCell(BoolVar[][] Y, BoolVar[][] SX) {
        Y[0][0] = SX[0][0];
        Y[1][0] = SX[2][2];
        Y[2][0] = SX[1][1];
        Y[3][0] = SX[3][3];

        Y[0][1] = SX[2][3];
        Y[1][1] = SX[0][1];
        Y[2][1] = SX[3][2];
        Y[3][1] = SX[1][0];

        Y[0][2] = SX[1][2];
        Y[1][2] = SX[3][0];
        Y[2][2] = SX[0][3];
        Y[3][2] = SX[2][1];

        Y[0][3] = SX[3][1];
        Y[1][3] = SX[1][3];
        Y[2][3] = SX[2][0];
        Y[3][3] = SX[0][2];
    }

    private void mixColumns(BoolVar[][] Z, BoolVar[][] Y) {
        for (int k = 0; k < 4; k++) {
            addXor(Y[0][k], Y[1][k], Y[2][k], Z[3][k]);
            addXor(Y[1][k], Y[2][k], Y[3][k], Z[0][k]);
            addXor(Y[2][k], Y[3][k], Y[0][k], Z[1][k]);
            addXor(Y[3][k], Y[0][k], Y[1][k], Z[2][k]);
        }
    }

    private void addXor(BoolVar... variables) {
        xorEquations.add(variables);
    }

    private void configure(IntVar[] nbActives, BoolVar[] flattenX) {
        Solver s = m.getSolver();
        s.setNoGoodRecordingFromRestarts();
        s.setRestarts(value -> s.getFailCount() > value, new LubyCutoffStrategy(20), 200);
        s.setNoGoodRecordingFromSolutions(flattenX);
        s.setSearch(
                Search.domOverWDegSearch(nbActives),
                Search.domOverWDegSearch(flattenX),
                Search.domOverWDegSearch(m.retrieveIntVars(true))
        );
        s.setSearch(Search.lastConflict(s.getSearch(),8));
    }

    // Midori Model
    public static void main(String[] args) {
        int r = 13;
        int sb = 13;
        int version = 128; // 64 or 128

        new Example(r, sb, version);
    }


}
