# Abstract XOR

Authors:
- [Loïc Rouquette](http://loicrouquette.fr)
- [Chirstine Solnon](http://perso.citi.insa-lyon.fr/csolnon)

This repository contains the source code for the Abstract XOR constraint published in [1].

Cite this code :

```bib
@InProceedings{10.1007/978-3-030-58475-7_33,
    author="Rouquette, Lo{\"i}c
    and Solnon, Christine",
    editor="Simonis, Helmut",
    title="abstractXOR: A global constraint dedicated to differential cryptanalysis",
    booktitle="Principles and Practice of Constraint Programming",
    year="2020",
    publisher="Springer International Publishing",
    address="Cham",
    pages="566--584",
    abstract="Constraint Programming models have been recently proposed to solve cryptanalysis problems for symmetric block ciphers such as AES. These models are more efficient than dedicated approaches but their design is difficult: straightforward models do not scale well and it is necessary to add advanced constraints derived from cryptographic properties. We introduce a global constraint which simplifies the modelling step and improves efficiency. We study its complexity, introduce propagators and experimentally evaluate them on two cryptanalysis problems (single-key and related-key) for two block ciphers (AES and Midori).",
    isbn="978-3-030-58475-7"
}
```

To run the project :

For `linux` and `mac os` :

```bash
./gradlew build
```

For `windows` :

```bash
gradlew.bat build
```

The project will be compiled to a `.jar` file that can be used as a dependency.

[1] :  [*abstractXOR*: A global constraint dedicated to differential cryptanalysis](https://link.springer.com/chapter/10.1007/978-3-030-58475-7_33) Loïc Rouquette, Christine Solnon